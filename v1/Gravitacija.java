import java.util.*;
import java.lang.Math;
public class Gravitacija {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("OIS je zakon!");


        System.out.print("Vnesite nadmorsko visino: ");
        double nadmorskaVisina=sc.nextDouble();

		System.out.println("Pospesek na "+nadmorskaVisina+" je:");
        System.out.println(izracun(nadmorskaVisina));


    }

    public static double izracun(double nadmorskaVisina){


        double G = 6.674*Math.pow(10,-11);
        double M = 5.972*Math.pow(10,24);
        double r = 6.371*Math.pow(10,6);

        double a = G*M/((r+nadmorskaVisina)*(r+nadmorskaVisina));

        return a;


    }

    /*double izpis(double izracun(nadmorskaVisina)) {
        System.out.println(nadmorskaVisina);
        System.out.println(izracun);
        return izracun;
    }*/
}